package br.com.itau;

public class Jogador {
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Rodada getHistorico() {
        return historico;
    }

    public void setHistorico(Rodada historico) {
        this.historico = historico;
    }

    public int getQuantidadeRodada() {
        return quantidadeRodada;
    }

    public Jogador(String nome) {
        this.nome = nome;
        this.quantidadeRodada = 0;
    }

    public void acumularQuantidadeRodada(){
        this.quantidadeRodada ++;
    }

    private String nome;
    private Rodada historico;
    private int quantidadeRodada;
}
