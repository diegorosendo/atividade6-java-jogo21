package br.com.itau;

public class Impressora {

    public static void exibirFimRodada(Rodada rodada) {
        int contador =0;
        System.out.println("Rodada: " + rodada.getJogador().getQuantidadeRodada() + "- Encerrada!");
        for (Carta carta: rodada.getCartas()) {
            contador ++;
            System.out.print("Carta: " + contador);
            System.out.print(" - ");
            System.out.print(carta.getNaipe());
            System.out.print(" - ");
            System.out.print(carta.getNumero().getKey());
            System.out.print(" - ");
            System.out.print(carta.getNumero().getValue());
            System.out.println("");
        }
        System.out.println("Total pontos: " + rodada.getPontuacao());
    }

    public static void exibirRodada(Rodada rodada) {
        int contador =0;
        System.out.println("-----------------------------------------");
        System.out.println("Jogador: " + rodada.getJogador().getNome());
        System.out.println("Rodada: " + rodada.getJogador().getQuantidadeRodada());
        for (Carta carta: rodada.getCartas()) {
            contador ++;
            System.out.print("Carta: " + contador);
            System.out.print(" - ");
            System.out.print(carta.getNaipe());
            System.out.print(" - ");
            System.out.print(carta.getNumero().getKey());
            System.out.print(" - ");
            System.out.print(carta.getNumero().getValue());
            System.out.println("");
        }
        System.out.println("Total pontos: " + rodada.getPontuacao());
        System.out.println("-----------------------------------------");
    }

    public static void exibirHistorico(Historico historico) {
        System.out.println("-----------------------------------------");
        System.out.println("Jogador: " + historico.getMelhorRodada().getJogador().getNome());
        System.out.println("Melhor Pontuação: " + historico.getMelhorRodada().getPontuacao());
        System.out.println("-----------------------------------------");
    }

}
