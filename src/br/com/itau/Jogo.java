package br.com.itau;

public class Jogo {

    public boolean inicializarSistema() {

        Jogador jogador = new Jogador(Escaner.inicializarSistema());
        Baralho baralho = new Baralho();
        Rodada rodada =  new Rodada(jogador);
        Historico historico = new Historico(jogador);

        int opcaoEscolhida;
            do {
            opcaoEscolhida = Escaner.exibirMenuSistema();
            switch (opcaoEscolhida) {
                case 1:
                    System.out.println("Opção escolhida: *** Sortear Nova Carta ***");
                    Carta carta = new Carta();
                    carta = baralho.sortearCarta();
                    if (rodada.acumularCartas(carta)){
                        Impressora.exibirRodada(rodada);
                    }else{
                        Impressora.exibirRodada(rodada);
                        Impressora.exibirFimRodada(rodada);
                        rodada = new Rodada(rodada.getJogador());
                    }

                    break;

                case 2:
                    System.out.println("Opção escolhida: *** Nova Rodada ***");
                    if (historico.guardarMelhorHistorico(rodada)){
                        System.out.println("Parabéns! Melhor rodada até o momento!");
                    }
                    rodada = new Rodada(rodada.getJogador());
                    break;

                case 9:
                    System.out.println("Opção escolhida: *** Encerrar Jogo ***");
                    Impressora.exibirHistorico(historico);
                    break;

                default:

            }
        }   while (opcaoEscolhida != 9) ;

        return true;

    }

}
