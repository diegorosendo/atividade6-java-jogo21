package br.com.itau;

import java.util.ArrayList;
import java.util.List;

public class Historico {
    public Historico(Jogador jogador) {
        this.rodadas = new ArrayList<Rodada>();
        this.melhorRodada = new Rodada(jogador);
    }

    private List<Rodada> rodadas;
    private Rodada melhorRodada;

    public Rodada getMelhorRodada() {
        return melhorRodada;
    }

    public boolean guardarMelhorHistorico(Rodada rodada){
        if (this.melhorRodada.getPontuacao() < rodada.getPontuacao() ){
            this.melhorRodada = rodada;
            return true;
        }
        return false;
    }
}
