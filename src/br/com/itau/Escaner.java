package br.com.itau;

import java.util.Scanner;

public class Escaner {
    public static String inicializarSistema() {
        System.out.println("Bem vindo ao Jogo de Cartas 21");
        System.out.println("Informe seu nome: ");

        Scanner scanner = new Scanner(System.in);
        return scanner.next();
    }

    public static int exibirMenuSistema() {
        System.out.println("Digite uma opção");
        System.out.println("1-Sortear Nova Carta");
        System.out.println("2-Nova Rodada");
        System.out.println("9-Encerrar Jogo");

        Scanner scanner = new Scanner(System.in);
        return scanner.nextInt();
    }

//    public static Contato incluirContato() {
//        Scanner scanner = new Scanner(System.in);
//        Contato contato = new Contato();
//
//        System.out.println("Nome: ");
//        contato.setNome(scanner.next());
//
//        System.out.println("Email:");
//        contato.setEmail(scanner.next());
//
//        System.out.println("Telefone:");
//        contato.setTelefone(scanner.next());
//
//        System.out.println("Contato adicionado com sucesso!:");
//
//        return contato;
//
//    }
//
//    public static String informarEmailImpressao() {
//        Scanner scanner = new Scanner(System.in);
//        System.out.println("Informe o email para imprimir contato: ");
//        return scanner.next();
//    }
//
//    public static String informarTelefoneImpressao() {
//        Scanner scanner = new Scanner(System.in);
//        System.out.println("Informe o telefone para imprimir contato: ");
//        return scanner.next();
//    }
//
//
//    public static String informarEmailRemocao() {
//        Scanner scanner = new Scanner(System.in);
//        System.out.println("Informe o email do contato a ser removido: ");
//        return scanner.next();
//    }
//
//
//    public static String informarTelefoneRemocao() {
//        Scanner scanner = new Scanner(System.in);
//        System.out.println("Informe o número de telefone do contato a ser removido: ");
//        return scanner.next();
//    }
}
