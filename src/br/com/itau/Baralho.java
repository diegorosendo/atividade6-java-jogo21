package br.com.itau;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Baralho {
    private List<Carta> cartas;

    public Baralho() {
        cartas = new ArrayList<Carta>(14*4);

        for (Naipe naipe: Naipe.values()) {
            for (Numero numero: Numero.values()){
                Carta carta = new Carta(naipe, numero);
                cartas.add(carta);
            }
        }


    }

    public Carta sortearCarta()
    {
        Random random = new Random();
        int numero = random.nextInt(this.cartas.size());

        Carta carta = new Carta();
        carta = this.cartas.get(numero);

        this.cartas.remove(carta);

        return carta;
    }

    public void listarCartas()
    {
        for (Carta carta:this.cartas) {
            System.out.print(carta.getNaipe());
            System.out.print(" - ");
            System.out.print(carta.getNumero().getKey());
            System.out.print(" - ");
            System.out.print(carta.getNumero().getValue());
            System.out.println("");
        }
    }
}
