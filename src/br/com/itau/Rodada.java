package br.com.itau;

import java.util.ArrayList;
import java.util.List;

public class Rodada {
    private int quantidadeCartasSorteadas;
    private int pontuacao;
    private List <Carta> cartas;
    private Jogador jogador;

    public Jogador getJogador() {
        return jogador;
    }

    public int getQuantidadeCartasSorteadas() {
        return quantidadeCartasSorteadas;
    }

    public int getPontuacao() {
        return pontuacao;
    }

    public List<Carta> getCartas() {
        return cartas;
    }

    public Rodada(Jogador jogador) {
        this.quantidadeCartasSorteadas =0;
        this.pontuacao = 0;
        this.cartas = new ArrayList<Carta>(0);
        this.jogador = jogador;
        jogador.acumularQuantidadeRodada();
    }

    public boolean acumularCartas(Carta carta){
        this.quantidadeCartasSorteadas ++;
        this.pontuacao += carta.getNumero().getValue();
        this.cartas.add(carta);
        if (this.pontuacao > 21){
            return false;
        }else{
            return true;
        }
    }
}
