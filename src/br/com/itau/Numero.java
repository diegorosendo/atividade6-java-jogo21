package br.com.itau;

public enum Numero {

    AIS("AIS", 1),
    DOIS("DOIS", 2),
    TRES("TRES", 3),
    QUATRO("QUATRO", 4),
    CINCO("CINCO", 5),
    SEIS("SEIS", 6),
    SETE("SETE", 7),
    OITO("OITO", 8),
    NOVE("NOVE", 9),
    DEZ("DEZ", 10),
    DAMA("DAMA", 10),
    VALET("VALET", 10),
    REI("REI", 10);

    private final String key;
    private final Integer value;

    Numero(String key, Integer value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public Integer getValue() {
        return value;
    }
}

